# Snake game

Desktop GUI game with snake and movement with arrows.

- Vision only around snakes eyes

- Navigation to food by sound volume

- Gradual adding of walls

- Persistent saving of reached high score
