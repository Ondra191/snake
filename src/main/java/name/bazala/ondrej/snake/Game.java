package name.bazala.ondrej.snake;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import name.bazala.ondrej.snake.enums.Direction;
import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.enums.GameState;
import name.bazala.ondrej.snake.objects.Candy;
import name.bazala.ondrej.snake.objects.GameObject;
import name.bazala.ondrej.snake.objects.Snake;
import name.bazala.ondrej.snake.objects.Wall;
import name.bazala.ondrej.snake.screens.*;
import name.bazala.ondrej.snake.util.Position;
import name.bazala.ondrej.snake.util.PositionUtil;
import name.bazala.ondrej.snake.util.VisiblePositions;
import sun.plugin2.os.windows.Windows;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game extends Canvas implements Runnable {

    public static int GAME_WIDTH = 1080;
    public static int GAME_HEIGHT = 1080;
    public static int TILE_SIZE = 12;

    private boolean visible = false;
    private JFrame jFrame;
    private Timer gameTimer = new Timer();
    private VisiblePositions visiblePositions;
    private Handler handler;
    private Hud hud;
    private static boolean isRunning = false;
    private double scale;
    private Snake snake;
    public static GameState GAME_STATE = GameState.START_SCREEN;
    private Map<Enum, Screen> screens = new HashMap<>();

    public Game() {
        init();
    }

    private void init() {
        Direction snakeDirection = Direction.EAST;
        Position snakePosition = new Position(((GAME_WIDTH / TILE_SIZE) / 2) * TILE_SIZE, ((GAME_HEIGHT / TILE_SIZE) / 2) * TILE_SIZE);
        visiblePositions = new VisiblePositions(snakePosition, snakeDirection);
        Snake.starting = true;
        Score.init();
        hud = new Hud(visiblePositions, gameTimer);
        handler = new Handler(visiblePositions);
        snake = new Snake(ObjectId.SNAKE, snakeDirection, snakePosition, handler, gameTimer);
        addAllObjects(snake);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        scale = Math.min(screenSize.getHeight() - (TILE_SIZE * 10), screenSize.getWidth() - (TILE_SIZE * 10)) / GAME_HEIGHT;
        addScreens();
        new javafx.embed.swing.JFXPanel();
        executeRunnable(Sounds.getSound("/sounds/menu_sound.mp3", "/sounds/game_sound.mp3",
                "/sounds/game_over_sound.mp3", "/sounds/score_sound.wav"));
        SwingUtilities.invokeLater(() -> new Window(this, snake));

    }

    private void addAllObjects(Snake snake) {
        handler.addObject(snake);

        handler.addObject(new Candy(ObjectId.CANDY, visiblePositions.getRandomInvisiblePosition(handler), handler));

        for (int i = 0; i < 50; i++) {
            handler.addObject(new Wall(ObjectId.WALL, PositionUtil.getRandomFreePosition(handler), handler));
        }
    }

    private void addScreens() {
        screens.put(GameState.START_SCREEN, new StartScreen(this));
        screens.put(GameState.PLAYING_SCREEN, new PlayingScreen(this, handler, hud));
        screens.put(GameState.HIGH_SCORE_SCREEN, new HighScoreScreen(this));
        screens.put(GameState.GAME_OVER_SCREEN, new GameOverScreen(this));
    }

    public void start() {
        isRunning = true;
        executeRunnable(this);
    }

    public void restart() {
        Score.score = 0;
        Snake.starting = true;
        for (Restart o : handler.getObjects()) {
            o.restart();
        }
        gameTimer.setTime(60);
        isRunning = true;
    }

    public static boolean getIsRunning() {
        return isRunning;
    }

    public static void stop() {
        Snake.starting = true;
        isRunning = false;
    }

    public void run() {
        final long second = 1000;
        long lastTime = System.nanoTime();
        double amountOfTicks = 20.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int fps = 0;
        boolean isTick = false;
        while(true) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while (delta >= 1) {
                update();
                --delta;
                isTick = true;
            }
            if (isTick) {
                render();
                isTick = false;
            }
            fps++;

            if (System.currentTimeMillis() - timer > second) {
                timer += second;
                if (!Snake.starting && GAME_STATE == GameState.PLAYING_SCREEN) {
                    gameTimer.decreaseSecond();
                }
                fps = 0;
            }
        }
    }

    private void update() {
        screens.get(GAME_STATE).update();
    }

    private void render() {
        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);
            return;
        }

        do {
            Graphics2D g = (Graphics2D) bs.getDrawGraphics();

            g.scale(scale, scale);

            screens.get(GAME_STATE).render(g);

            g.dispose();
        } while (bs.contentsLost());
        bs.show();
        if (!visible) {
            SwingUtilities.invokeLater(() -> jFrame.setVisible(true));
            visible = true;
        }
    }

    private void executeRunnable(Runnable runnable) {
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(runnable);
        executor.shutdown();
    }

    public void setJFrame(JFrame jFrame) {
        this.jFrame = jFrame;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        new Game();
    }
}
