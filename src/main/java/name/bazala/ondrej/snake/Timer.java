package name.bazala.ondrej.snake;

import name.bazala.ondrej.snake.enums.GameState;

public class Timer {

    private int time = 60;

    public void decreaseSecond() {
        if (time > 0) {
            --time;
        }
        if (time == 0) {
            Game.GAME_STATE = GameState.GAME_OVER_SCREEN;
            Game.stop();
        }
    }

    public int getTime() {
        return time;
    }

    public void addBonusTime(int time) {
        this.time += time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
