package name.bazala.ondrej.snake.util;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Handler;
import name.bazala.ondrej.snake.enums.Direction;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class VisiblePositions {

    private static final int NUM_1 = 40;
    private static final int NUM_2 = 40;

    private Position snakePosition;
    private Direction snakeDirection;
    private Set<Position> positions = new HashSet<>();

    public VisiblePositions(Position snakePosition, Direction snakeDirection) {
        this.snakePosition = snakePosition;
        this.snakeDirection = snakeDirection;
        setVisiblePositions(snakePosition, snakeDirection);
    }

    public void setVisiblePositions(Position snakePosition, Direction snakeDirection) {
        this.snakePosition = snakePosition;
        this.snakeDirection = snakeDirection;


        positions.clear();
        Position position;

        switch (snakeDirection) {
            case NORTH:
                position = new Position(snakePosition.getX() - Game.TILE_SIZE * (NUM_1 / 2), snakePosition.getY() - Game.TILE_SIZE * (NUM_2 - 10));
                findVisiblePositions(NUM_1, NUM_2, position);
                break;
            case WEST:
                position = new Position(snakePosition.getX() - Game.TILE_SIZE * (NUM_2 - 10), snakePosition.getY() - Game.TILE_SIZE * (NUM_1 / 2));
                findVisiblePositions(NUM_2, NUM_1, position);
                break;
            case EAST:
                position = new Position(snakePosition.getX() - Game.TILE_SIZE * (NUM_2 - 30), snakePosition.getY() - Game.TILE_SIZE * (NUM_1 / 2));
                findVisiblePositions(NUM_2, NUM_1, position);
                break;
            case SOUTH:
                position = new Position(snakePosition.getX() - Game.TILE_SIZE * (NUM_1 / 2), snakePosition.getY() - Game.TILE_SIZE * (NUM_2 - 30));
                findVisiblePositions(NUM_1, NUM_2, position);
                break;
        }


    }

    private void findVisiblePositions(int col, int row, Position position) {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                int x = position.getX() + Game.TILE_SIZE * j;
                int y = position.getY() + Game.TILE_SIZE * i;
                int horizontalLength = Math.abs(x - snakePosition.getX() + 6);
                int verticalLength = Math.abs(y - snakePosition.getY() + 6);
                if (horizontalLength * horizontalLength + verticalLength * verticalLength > 30_000) {
                    continue;
                }
                boolean xNotOnMap = x < 0 || x >= Game.GAME_WIDTH;
                boolean yNotOnMap = y < 0 || y >= Game.GAME_WIDTH;
                if (xNotOnMap && !yNotOnMap) {
                    x += Integer.signum(position.getX()) * (-1) * Game.GAME_WIDTH;
                }
                if (yNotOnMap && !xNotOnMap) {
                    y += Integer.signum(position.getY()) * (-1) * Game.GAME_HEIGHT;
                }
                Position newPosition = new Position(x, y);
                positions.add(newPosition);
            }
        }
    }

    public void setSnakePosition(Position snakePosition) {
        this.snakePosition = snakePosition;
    }

    public void setSnakeDirection(Direction snakeDirection) {
        this.snakeDirection = snakeDirection;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    public boolean arePositionsInVisiblePositions(Collection<Position> toCheckPositions) {
        for (Position position : toCheckPositions) {
            if (this.positions.contains(position)) {
                return true;
            }
        }
        return false;
    }

    public boolean isPositionInVisiblePositions(Position position) {
        return  positions.contains(position);
    }

    public Position getRandomInvisiblePosition(Handler handler) {
        Position position = PositionUtil.getRandomFreePosition(handler);
        while (isPositionInVisiblePositions(position)) {
            position = PositionUtil.getRandomFreePosition(handler);
        }
        return position;
    }
}
