package name.bazala.ondrej.snake.util;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Handler;
import name.bazala.ondrej.snake.util.Position;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class PositionUtil {


    public static Position getRandomFreePosition(Handler handler) {
        Position position;
        do {
            position = getRandomPosition();
        } while(!isPositionFree(position, handler));
        return position;
    }

    public static Position getRandomPosition() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return new Position(
                random.nextInt(Game.GAME_WIDTH / Game.TILE_SIZE) * Game.TILE_SIZE,
                random.nextInt(Game.GAME_HEIGHT / Game.TILE_SIZE) * Game.TILE_SIZE
        );
    }

    public static boolean isPositionFree(Position position, Handler handler) {
        Set<Position> fullPositions = handler.getAllObjectsPositions();
        return !fullPositions.contains(position);
    }

    public static boolean arePositionsFree(List<Position> positions, Handler handler) {
        Set<Position> occupiedPositions = handler.getAllObjectsPositions();
        for (Position position : positions) {
            if (occupiedPositions.contains(position)) {
                return false;
            }
        }
        return true;
    }
}
