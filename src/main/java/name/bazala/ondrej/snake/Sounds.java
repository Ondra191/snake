package name.bazala.ondrej.snake;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import name.bazala.ondrej.snake.objects.Snake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class Sounds {

    public static double volume = 0.05;
    public static boolean pressed = false;
    public static boolean eaten = false;

    public static Runnable getSound(String... path) {
        return () -> playSound(path);
    }

    private static void playSound(String... path) {
        Thread.currentThread().setName("sound-thread");

        List<MediaPlayer> players = new ArrayList<>(6);

        for (String p : path) {
            Media music = new Media(getURL(p).toString());
            MediaPlayer player = new MediaPlayer(music);
            players.add(player);
        }

        players.get(1).setCycleCount(MediaPlayer.INDEFINITE);


        while (true) {
            switch (Game.GAME_STATE) {
                case GAME_OVER_SCREEN:
                    players.get(1).stop();
                    players.get(2).setVolume(1);
                    players.get(2).play();
                    playBeep(players.get(0));
                    break;
                case HIGH_SCORE_SCREEN:
                case START_SCREEN:
                    players.get(2).stop();
                    players.get(1).stop();
                    playBeep(players.get(0));
                    break;
                case PLAYING_SCREEN:
                    players.get(2).stop();
                    players.get(1).setVolume(volume);
                    if (Snake.starting) {
                        players.get(1).stop();
                    } else {
                        players.get(1).play();
                        if (eaten) {
                            players.get(3).setVolume(1);
                            players.get(3).stop();
                            players.get(3).play();
                            eaten = false;
                        }
                    }
                    break;
            }
        }
    }

    private static void playBeep(MediaPlayer player) {
        if (pressed) {
            player.setVolume(1);
            player.stop();
            player.play();
            pressed = false;
        }
    }

    private static URI getURL(String path) {
        URI uri = null;
        try {
            uri = new URI(Window.class.getResource(path).toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (uri == null) throw new NullPointerException();
        return uri;
    }
}
