package name.bazala.ondrej.snake;

import name.bazala.ondrej.snake.objects.Snake;

import java.util.Arrays;
import java.util.List;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Window {

    public static JFrame jFrame;

    Window(Game game, Snake snake) {
        init(game, snake);
    }

    private void init(Game game, Snake snake) {
        jFrame = new JFrame("Snake");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double scale = Math.min(screenSize.getHeight(), screenSize.getWidth()) / Game.GAME_HEIGHT;
        Dimension frameSize = new Dimension((int) (scale * Game.GAME_WIDTH) - (Game.TILE_SIZE * 10),(int) (scale * Game.GAME_HEIGHT) - (Game.TILE_SIZE * 10));
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setResizable(false);
        jFrame.setIconImages(getIcons());

        game.setFocusable(true);
        game.requestFocus();
        game.setPreferredSize(frameSize);

        jFrame.add(game);
        jFrame.pack();
        jFrame.setLocation((int) (screenSize.getWidth() / 2 - frameSize.getWidth() / 2), 0);
        game.setBackground(Color.BLACK);
        game.setJFrame(jFrame);
        game.start();
    }

    private List<Image> getIcons() {
        List<String> imagesPaths = Arrays.asList("/icons/snake-icon-32.png", "/icons/snake-icon-24.png",
                "/icons/snake-icon-16.png", "/icons/snake-icon-48.png",
                "/icons/snake-icon-64.png", "/icons/snake-icon-92.png",
                "/icons/snake-icon-96.png", "/icons/snake-icon-128.png");
        List<Image> icons = new ArrayList<>();
        for (String path : imagesPaths) {
            icons.add(getImage(path));
        }
        return icons;
    }

    private Image getImage(String path) {
        return new ImageIcon(Game.class.getResource(path)).getImage();
    }
}
