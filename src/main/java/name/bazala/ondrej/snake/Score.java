package name.bazala.ondrej.snake;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Score {

    public static int score = 0;
    public static int highScore = 0;
    private static final String filePath = System.getenv("APPDATA") + "/Snake game/snake_score.data";
    private static final String foldersPath = System.getenv("APPDATA") + "/Snake game/";

    public static void init() {
        if (!Paths.get(foldersPath).toFile().exists()) {
            try {
                Files.createDirectories(Paths.get(foldersPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!Paths.get(filePath).toFile().exists()) {
            try (FileWriter writer = new FileWriter(filePath)) {
                writer.write(score + "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        highScore = readHighScore();
    }

    public static void writeHighScore() {
        if (highScore < score) {
            try (FileWriter writer = new FileWriter(filePath)) {
                writer.write(score + "");
                highScore = score;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static int readHighScore() {
        try (Scanner scanner = new Scanner(Paths.get(filePath))) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

}
