package name.bazala.ondrej.snake;

import name.bazala.ondrej.snake.util.Position;
import name.bazala.ondrej.snake.util.VisiblePositions;

import java.awt.*;


public class Hud {

    private VisiblePositions visiblePositions;
    private Position[] positions = new Position[] {new Position(0, 0), new Position(Game.TILE_SIZE, 0),
            new Position(0, Game.TILE_SIZE), new Position(Game.TILE_SIZE, Game.TILE_SIZE)};
    private Timer gameTimer;

    public Hud(VisiblePositions visiblePositions, Timer gameTimer) {
        this.visiblePositions = visiblePositions;
        this.gameTimer = gameTimer;
    }

    public void render(Graphics g) {
        g.setFont(new Font("Helvetica", Font.PLAIN, 20));
        g.setColor(Color.WHITE);
        for (Position position : positions) {
            if (visiblePositions.getPositions().contains(position)) {
                g.setColor(Color.BLACK);
                break;
            }
        }
        g.drawString(Score.score + "", 4, 20);
        g.drawString(gameTimer.getTime() + "", 4, 40);
    }
}

