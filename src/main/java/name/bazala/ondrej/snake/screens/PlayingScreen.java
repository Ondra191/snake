package name.bazala.ondrej.snake.screens;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Handler;
import name.bazala.ondrej.snake.Hud;
import name.bazala.ondrej.snake.input.PlayingScreenKeyInput;
import name.bazala.ondrej.snake.input.StartScreenKeyInput;
import name.bazala.ondrej.snake.objects.Snake;
import name.bazala.ondrej.snake.util.Position;
import name.bazala.ondrej.snake.util.VisiblePositions;

import java.awt.*;
import java.awt.event.KeyListener;

public class PlayingScreen implements Screen {
    private Game game;
    private Handler handler;
    private Hud hud;
    private VisiblePositions visiblePositions;

    public PlayingScreen(Game game, Handler handler, Hud hud) {
        this.game = game;
        this.handler = handler;
        this.hud = hud;
        this.visiblePositions = handler.getVisiblePositions();
        game.addKeyListener(new PlayingScreenKeyInput(game, handler.getSnake()));

    }

    @Override
    public void update() {
         handler.updateAll();
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(new Color(0, 0, 0));
        g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);

        for (Position position : visiblePositions.getPositions()) {
            g.setColor(Color.WHITE);
            g.fillRect(position.getX(), position.getY(), Game.TILE_SIZE, Game.TILE_SIZE);
        }

        handler.renderAll(g);

        hud.render(g);

        int width;
        if (Snake.starting) {
            g.setColor(Color.WHITE);
            width = g.getFontMetrics().stringWidth("press SPACE to start the game");
            g.drawString("press SPACE to start the game", (Game.GAME_WIDTH / 2) - (width / 2), (Game.GAME_HEIGHT / 2) - 200);
        }


    }
}
