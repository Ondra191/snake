package name.bazala.ondrej.snake.screens;

import java.awt.*;

public interface Screen {
    void update();
    void render(Graphics2D g);
}
