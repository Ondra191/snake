package name.bazala.ondrej.snake.screens;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Score;
import name.bazala.ondrej.snake.input.HighScoreKeyInput;

import java.awt.*;

public class HighScoreScreen implements Screen {

    private Game game;

    public HighScoreScreen(Game game) {
        this.game = game;
        game.addKeyListener(new HighScoreKeyInput());
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);

        int width;
        g.setColor(Color.GREEN);
        g.setFont(new Font("Helvetica", Font.BOLD, 60));
        width = g.getFontMetrics().stringWidth("YOUR HIGHEST SCORE WAS:");
        g.drawString("YOUR HIGHEST SCORE WAS:", (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2 - 200);

        g.setFont(new Font("Helvetica", Font.BOLD, 100));
        width = g.getFontMetrics().stringWidth(Score.highScore + "");
        g.drawString(Score.highScore + "", (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2);

        g.setFont(new Font("Helvetica", Font.BOLD, 30));
        width = g.getFontMetrics().stringWidth("press ESCAPE to go back to menu");
        g.drawString("press ESCAPE to go back to menu", (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2 + 200);
    }
}
