package name.bazala.ondrej.snake.screens;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.enums.SelectedMenuOption;
import name.bazala.ondrej.snake.input.StartScreenKeyInput;
import name.bazala.ondrej.snake.objects.Snake;

import java.awt.*;

public class StartScreen implements Screen {

    private Game game;
    private SelectedMenuOption selected = SelectedMenuOption.PLAY;

    public StartScreen(Game game) {
        this.game = game;
        game.addKeyListener(new StartScreenKeyInput(this));
    }

    @Override
    public void update() {
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);

        int width = 0;
        int a = (Game.GAME_WIDTH / 2) - (width / 2);
        String playText = Snake.starting ? "PLAY" : "RESUME";

        g.setColor(Color.GREEN);
        g.setFont(new Font("Helvetica", Font.BOLD, 60));
        width = g.getFontMetrics().stringWidth(playText);
        g.drawString(playText, (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2 - 200);
        width = g.getFontMetrics().stringWidth("HIGHSCORE");
        g.drawString("HIGHSCORE", (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2);
        width = g.getFontMetrics().stringWidth("QUIT");
        g.drawString("QUIT", (Game.GAME_WIDTH / 2) - (width / 2), Game.GAME_HEIGHT / 2 + 200);

        width = g.getFontMetrics().stringWidth(selected.toString());
        if (selected.toString().equals("PLAY")) {
            width = g.getFontMetrics().stringWidth(playText);
        }
        g.drawRect((Game.GAME_WIDTH / 2) - (width / 2) - 5, (Game.GAME_HEIGHT / 2) - 52 + selected.getRenderPosition(), width + 10, 60);
    }

    public SelectedMenuOption getSelected() {
        return selected;
    }

    public void setSelected(SelectedMenuOption selected) {
        this.selected = selected;
    }
}
