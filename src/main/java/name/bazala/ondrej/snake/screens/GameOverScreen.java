package name.bazala.ondrej.snake.screens;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Score;
import name.bazala.ondrej.snake.input.GameOverScreenKeyInput;

import java.awt.*;

public class GameOverScreen implements Screen {

    private int score;
    private Game game;

    public GameOverScreen(Game game) {
        this.game = game;
        game.addKeyListener(new GameOverScreenKeyInput(game));
    }

    @Override
    public void update() {
        Score.writeHighScore();
        if (!Game.getIsRunning()) {
            score = Score.score;
            game.restart();
        }
    }

    @Override
    public void render(Graphics2D g) {
        int width;
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);
        g.setColor(Color.RED);
        g.setFont(new Font("Helvetica", Font.BOLD, 80));
        width = g.getFontMetrics().stringWidth("GAME OVER");
        g.drawString("GAME OVER", (Game.GAME_WIDTH / 2) - (width / 2), (Game.GAME_HEIGHT / 2) - 100);
        g.setFont(new Font("Helvetica", Font.PLAIN, 30));
        width = g.getFontMetrics().stringWidth(("Score: " + score));
        g.drawString("Score: " + score, (Game.GAME_WIDTH / 2) - (width / 2), (Game.GAME_HEIGHT / 2));

        width = g.getFontMetrics().stringWidth("press SPACE for another game");
        g.drawString("press SPACE for another game", (Game.GAME_WIDTH / 2) - (width / 2), (Game.GAME_HEIGHT / 2) + 100);
        width = g.getFontMetrics().stringWidth("press ESCAPE to go to start menu");
        g.drawString("press ESCAPE to go to start menu", (Game.GAME_WIDTH / 2) - (width / 2), (Game.GAME_HEIGHT / 2) + 200);
    }




}
