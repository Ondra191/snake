package name.bazala.ondrej.snake.input;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Sounds;
import name.bazala.ondrej.snake.enums.Direction;
import name.bazala.ondrej.snake.enums.GameState;
import name.bazala.ondrej.snake.objects.Snake;
import name.bazala.ondrej.snake.util.Position;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PlayingScreenKeyInput extends KeyAdapter {

    private Game game;
    private Snake snake;
    private Position lastPosition = new Position(-1, -1);
    private boolean restartFlag;

    public PlayingScreenKeyInput(Game game, Snake snake) {
        this.game = game;
        this.snake = snake;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (Game.GAME_STATE != GameState.PLAYING_SCREEN) {
            return;
        }

        int keyCode = e.getKeyCode();


        if (!lastPosition.equals(snake.getPosition())) {
            setDirection(keyCode);
            lastPosition = snake.getPosition();
        }
        if (keyCode == KeyEvent.VK_SPACE && Game.getIsRunning()) Snake.starting = false;

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (Game.GAME_STATE != GameState.PLAYING_SCREEN) return;
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_ESCAPE) {
            Sounds.pressed = true;
            Game.GAME_STATE = GameState.START_SCREEN;
        }
    }

    private void setDirection(int keyCode) {
        if (snake.getDirection() == Direction.WEST || snake.getDirection() == Direction.EAST) {
            if (keyCode == KeyEvent.VK_UP) {
                snake.setDirection(Direction.NORTH);
            } else if (keyCode == KeyEvent.VK_DOWN) {
                snake.setDirection(Direction.SOUTH);
            }
        } else {
            if (keyCode == KeyEvent.VK_LEFT) {
                snake.setDirection(Direction.WEST);
            } else if (keyCode == KeyEvent.VK_RIGHT) {
                snake.setDirection(Direction.EAST);
            }
        }
    }
}
