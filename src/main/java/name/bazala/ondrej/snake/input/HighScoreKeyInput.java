package name.bazala.ondrej.snake.input;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Sounds;
import name.bazala.ondrej.snake.enums.GameState;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class HighScoreKeyInput extends KeyAdapter {
    @Override
    public void keyReleased(KeyEvent e) {
        if (Game.GAME_STATE != GameState.HIGH_SCORE_SCREEN) return;
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_ESCAPE) {
            Sounds.pressed = true;
            Game.GAME_STATE = GameState.START_SCREEN;
        }
    }
}
