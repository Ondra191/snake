package name.bazala.ondrej.snake.input;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Sounds;
import name.bazala.ondrej.snake.enums.GameState;
import name.bazala.ondrej.snake.objects.Snake;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GameOverScreenKeyInput extends KeyAdapter {

    Game game;

    public GameOverScreenKeyInput (Game game) {
        this.game = game;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (Game.GAME_STATE != GameState.GAME_OVER_SCREEN) return;
        int keyCode = e.getKeyCode();


        if (keyCode == KeyEvent.VK_ESCAPE) {
            Sounds.pressed = true;
            Game.GAME_STATE = GameState.START_SCREEN;
        }

        if (keyCode == KeyEvent.VK_SPACE) {
            Sounds.pressed = true;
            Game.GAME_STATE = GameState.PLAYING_SCREEN;
        }

    }
}
