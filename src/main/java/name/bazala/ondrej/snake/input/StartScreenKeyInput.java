package name.bazala.ondrej.snake.input;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Sounds;
import name.bazala.ondrej.snake.enums.GameState;
import name.bazala.ondrej.snake.screens.StartScreen;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class StartScreenKeyInput extends KeyAdapter {

    private StartScreen startScreen;

    public StartScreenKeyInput(StartScreen startScreen) {
        this.startScreen = startScreen;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (Game.GAME_STATE != GameState.START_SCREEN) return;
        int keyCode = e.getKeyCode();

        if (keyCode == KeyEvent.VK_UP) {
            Sounds.pressed = true;
            startScreen.setSelected(startScreen.getSelected().getUpperOption());
        }

        if (keyCode == KeyEvent.VK_DOWN) {
            Sounds.pressed = true;
            startScreen.setSelected(startScreen.getSelected().getBottomOption());
        }

        if (keyCode == KeyEvent.VK_ENTER || keyCode == KeyEvent.VK_SPACE) {
            Sounds.pressed = true;
            Game.GAME_STATE = startScreen.getSelected().getState();
        }

        if (keyCode == KeyEvent.VK_ESCAPE) {
            Sounds.pressed = true;
            System.exit(0);
        }
    }


}
