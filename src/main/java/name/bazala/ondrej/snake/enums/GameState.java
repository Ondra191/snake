package name.bazala.ondrej.snake.enums;

public enum GameState {
    START_SCREEN,
    PLAYING_SCREEN,
    HIGH_SCORE_SCREEN,
    GAME_OVER_SCREEN
}
