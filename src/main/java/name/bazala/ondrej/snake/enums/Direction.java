package name.bazala.ondrej.snake.enums;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.objects.Snake;
import name.bazala.ondrej.snake.util.Position;

import java.util.function.Function;

public enum Direction {
    NORTH(snake -> new Position(snake.getPosition().getX(), snake.getPosition().getY() - Game.TILE_SIZE)),
    WEST(snake -> new Position(snake.getPosition().getX() - Game.TILE_SIZE, snake.getPosition().getY())),
    EAST(snake -> new Position(snake.getPosition().getX() + Game.TILE_SIZE, snake.getPosition().getY())),
    SOUTH(snake -> new Position(snake.getPosition().getX(), snake.getPosition().getY() + Game.TILE_SIZE));

    private Function<Snake, Position> function;

    Direction(Function<Snake, Position> function) {
        this.function = function;
    }

    public Position getNewPosition(Snake snake) {
        return function.apply(snake);
    }
}
