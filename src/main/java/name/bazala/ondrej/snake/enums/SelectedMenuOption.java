package name.bazala.ondrej.snake.enums;

import java.util.function.Supplier;

public enum SelectedMenuOption {
    PLAY(0, -200, () -> GameState.PLAYING_SCREEN),
    HIGHSCORE(1, 0, () -> GameState.HIGH_SCORE_SCREEN),
    QUIT(2, 200, () -> {
        System.exit(0); return null;
    });

    private static int selectedPosition = 0;
    private int position;
    private int renderPosition;
    private Supplier<GameState> supplier;

    SelectedMenuOption(int position, int relativePosition, Supplier<GameState> supplier) {
        this.position = position;
        this.renderPosition = relativePosition;
        this.supplier = supplier;
    }

    public int getRenderPosition() {
        return renderPosition;
    }

    public GameState getState() {
        return supplier.get();
    }

    public SelectedMenuOption getUpperOption() {
        selectedPosition = (selectedPosition - 1) % SelectedMenuOption.values().length;
        if (selectedPosition < 0) selectedPosition = SelectedMenuOption.values().length - 1;
        for (SelectedMenuOption s : SelectedMenuOption.values()) {
            if (selectedPosition == s.position) {
                return s;
            }
        }
        return null;
    }

    public SelectedMenuOption getBottomOption() {
        selectedPosition = (selectedPosition + 1) % SelectedMenuOption.values().length;
        for (SelectedMenuOption s : SelectedMenuOption.values()) {
            if (selectedPosition == s.position) {
                return s;
            }
        }
        return null;
    }
}
