package name.bazala.ondrej.snake.enums;

public enum ObjectId {
    SNAKE, CANDY, WALL;
}
