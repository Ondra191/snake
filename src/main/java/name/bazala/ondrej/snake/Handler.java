package name.bazala.ondrej.snake;

import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.objects.*;
import name.bazala.ondrej.snake.util.Position;
import name.bazala.ondrej.snake.util.VisiblePositions;

import java.awt.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Handler {

    private LinkedList<GameObject> objects = new LinkedList<>();
    private Set<GameObject> toAddObjects = new HashSet<>();
    private Set<GameObject> toRemoveObjects = new HashSet<>();

    private VisiblePositions visiblePositions;

    public Handler(VisiblePositions visiblePositions) {
        this.visiblePositions = visiblePositions;
    }

    public void updateAll() {
        for (GameObject object : objects) {
            object.update();
        }
        if (!toRemoveObjects.isEmpty()) {
            objects.removeAll(toRemoveObjects);
            toRemoveObjects.clear();
        }
        if (!toAddObjects.isEmpty()) {
            objects.addAll(toAddObjects);
            toAddObjects.clear();
        }
    }

    public void renderAll(Graphics g) {
        for (GameObject object : objects) {
            object.render(g);
        }
    }

    public void addObject(GameObject object) {
        objects.add(object);
    }

    public void removeObject(GameObject object) {
        objects.remove(object);
    }

    public LinkedList<GameObject> getObjects() {
        return objects;
    }

    public void addLaterObject(GameObject object) {
        toAddObjects.add(object);
    }

    public void removeLaterObject(GameObject object) {
        toRemoveObjects.add(object);
    }

    public Set<Position> getAllObjectsPositions(){
        Set<Position> positions = new HashSet<>();
        for (GameObject o : objects) {
            if (o.getId() == ObjectId.SNAKE) {
                for (Snake.PartOfBody part : ((Snake) o).getBody()) {
                    positions.add(part.getPosition());
                }
            } else if (o.getId() == ObjectId.WALL) {
                positions.addAll(((Wall) o).getPositions());
            } else {
                positions.add(o.getPosition());
            }
        }
        return positions;
    }

    public VisiblePositions getVisiblePositions() {
        return visiblePositions;
    }

    public Snake getSnake() {
        for (GameObject object : objects) {
            if (object.getId() == ObjectId.SNAKE) {
                return (Snake) object;
            }
        }
        return null;
    }

    public Candy getCandy() {
        for (GameObject object : objects) {
            if (object.getId() == ObjectId.CANDY) {
                return (Candy) object;
            }
        }
        return null;
    }
}
