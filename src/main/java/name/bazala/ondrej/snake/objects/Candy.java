package name.bazala.ondrej.snake.objects;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Handler;
import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.util.Position;

import java.awt.*;

public class Candy extends GameObject {

    private Handler handler;

    public Candy(ObjectId id, Position position, Handler handler) {
        super(id, position);
        this.handler = handler;
    }

    @Override
    public void update() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.RED);
        if (handler.getVisiblePositions().getPositions().contains(getPosition())) {
            g.fillOval(getPosition().getX(), getPosition().getY(), Game.TILE_SIZE, Game.TILE_SIZE);
        }
    }

    public void restart() {
        setPosition(handler.getVisiblePositions().getRandomInvisiblePosition(handler));
    }
}
