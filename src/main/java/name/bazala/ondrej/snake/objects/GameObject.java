package name.bazala.ondrej.snake.objects;

import name.bazala.ondrej.snake.Restart;
import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.util.Position;

import java.awt.*;

public abstract class GameObject implements Restart {
    private ObjectId id;
    private Position position;

    public GameObject(ObjectId id, Position position) {
        this.id = id;
        this.position = position;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public abstract void update();
    public abstract void render(Graphics g);

}
