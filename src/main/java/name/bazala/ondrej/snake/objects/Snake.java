package name.bazala.ondrej.snake.objects;

import name.bazala.ondrej.snake.*;
import name.bazala.ondrej.snake.Timer;
import name.bazala.ondrej.snake.enums.Direction;
import name.bazala.ondrej.snake.enums.GameState;
import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.util.Position;
import name.bazala.ondrej.snake.util.PositionUtil;


import java.awt.*;
import java.util.*;

import static name.bazala.ondrej.snake.Game.*;

public class Snake extends GameObject {

    private Direction direction;
    private LinkedList<PartOfBody> body;
    private Set<PartOfBody> set;
    private Handler handler;
    public static boolean starting = true;
    private Timer gameTimer;

    public Snake(ObjectId objectId, Direction direction, Position position, Handler handler, Timer gameTimer) {
        super(objectId, position);
        this.direction = direction;
        this.body = new LinkedList<>();
        this.set = new HashSet<>();
        fillSnake();
        this.handler = handler;
        this.gameTimer = gameTimer;
    }

    private void fillSnake() {
        body.add(new PartOfBody(getPosition()));
        for (int i = 0; i < 5; i++) {
            Position newPosition = new Position(body.get(i).getPosition().getX() - TILE_SIZE, getPosition().getY());
            body.add(new PartOfBody(newPosition));
        }
        set.addAll(body);
    }

    @Override
    public void update() {
        if (starting) {
            return;
        }
        Position newPosition = direction.getNewPosition(this);
        this.setPosition(newPosition);
        checkBorders(newPosition);
        setMusicVolume(handler.getCandy());
        set.remove(body.getLast());
        body.removeLast();
        checkCollision(newPosition);
        body.addFirst(new PartOfBody(newPosition));
        set.add(body.getFirst());
        handler.getVisiblePositions().setVisiblePositions(getPosition(), getDirection());
    }

    private void checkBorders(Position position) {
        if (position.getX() < 0 || position.getX() >= GAME_WIDTH) {
            position.setX(position.getX() + (Integer.signum(position.getX()) * (-1) * GAME_WIDTH));
        }
        if (position.getY() < 0 || position.getY() >= GAME_HEIGHT) {
            position.setY(position.getY() + (Integer.signum(position.getY()) * (-1) * GAME_HEIGHT));
        }
    }

    private void checkCollision(Position position) {
        if (set.contains(new PartOfBody(position))) {
            Game.GAME_STATE = GameState.GAME_OVER_SCREEN;
            Game.stop();
        }
        for (GameObject o : handler.getObjects()) {
            if (o.getId() == ObjectId.CANDY && position.equals(o.getPosition())) {
                o.setPosition(handler.getVisiblePositions().getRandomInvisiblePosition(handler));
                addPartOfBody();
                Sounds.eaten = true;
                gameTimer.addBonusTime(10);
                if (handler.getObjects().size() < 252) {
                    handler.addLaterObject(new Wall(ObjectId.WALL, handler.getVisiblePositions().getRandomInvisiblePosition(handler), handler));
                }
                ++Score.score;
            } else if (o.getId() == ObjectId.WALL && ((Wall) o).getPositions().contains(position)) {
                Game.GAME_STATE = GameState.GAME_OVER_SCREEN;
                Game.stop();
            }
        }
    }

    private void setMusicVolume(GameObject object) {
        int xWidth = Math.abs(getPosition().getX() - object.getPosition().getX());
        int yWidth = Math.abs(getPosition().getY() - object.getPosition().getY());
        double distance = xWidth * xWidth + yWidth * yWidth;
        double volume = (1.0 - (distance / 1_142_124));
        Sounds.volume = musicSwitch(volume);
    }

    private double musicSwitch(double volume) {
        if (volume > 0.97d) return 1d;
        if (volume > 0.92d) return 0.9d;
        if (volume > 0.86d) return 0.8d;
        if (volume > 0.80d) return 0.7d;
        if (volume > 0.76d) return 0.6d;
        if (volume > 0.72d) return 0.5d;
        if (volume > 0.65d) return 0.4d;
        if (volume > 0.55d) return 0.3d;
        if (volume > 0.35d) return 0.2d;
        return 0.1;
    }

    private void addPartOfBody() {
        Position position = body.getLast().getPosition();
        body.add(new PartOfBody(position));
    }

    @Override
    public void render(Graphics g) {
        Iterator it = body.iterator();

        g.setColor(Color.YELLOW);
        Position p = ((PartOfBody) it.next()).getPosition();
        g.fillRect(p.getX(), p.getY(), TILE_SIZE, TILE_SIZE);

        g.setColor(new Color(0, 160, 0));
        while (it.hasNext()) {
            p = ((PartOfBody) it.next()).getPosition();
            if (handler.getVisiblePositions().isPositionInVisiblePositions(p)) {
                g.fillRect(p.getX(), p.getY(), TILE_SIZE, TILE_SIZE);
            }
        }
    }

    public void restart() {
        Direction snakeDirection = Direction.EAST;
        Position snakePosition = new Position(((GAME_WIDTH / TILE_SIZE) / 2) * TILE_SIZE, ((GAME_HEIGHT / TILE_SIZE) / 2) * TILE_SIZE);
        setPosition(snakePosition);
        setDirection(snakeDirection);
        this.body = new LinkedList<>();
        this.set = new HashSet<>();
        fillSnake();
        starting = true;
        handler.getVisiblePositions().setVisiblePositions(getPosition(), getDirection());
    }

    public LinkedList<PartOfBody> getBody() {
        return body;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public static class PartOfBody {
        Position position;

        private PartOfBody(Position position) {
            this.position = position;
        }

        public Position getPosition() {
            return position;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PartOfBody that = (PartOfBody) o;
            return Objects.equals(position, that.position);
        }

        @Override
        public int hashCode() {
            return Objects.hash(position);
        }
    }
}
