package name.bazala.ondrej.snake.objects;

import name.bazala.ondrej.snake.Game;
import name.bazala.ondrej.snake.Handler;
import name.bazala.ondrej.snake.util.PositionUtil;
import name.bazala.ondrej.snake.enums.ObjectId;
import name.bazala.ondrej.snake.util.Position;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Wall extends GameObject {

    private List<Position> positions = new ArrayList<>();
    private Handler handler;
    private long timer;
    private boolean isTimerSet = false;

    public Wall(ObjectId id, Position position, Handler handler) {
        super(id, position);
        this.handler = handler;
        makeWall();
    }

    @Override
    public void update() {
        if (!isTimerSet) {
            timer = System.currentTimeMillis();
            isTimerSet = true;
        }
        if (System.currentTimeMillis() - timer > 5_000) {
            timer = System.currentTimeMillis();
            if (handler.getVisiblePositions().arePositionsInVisiblePositions(positions)) {
                return;
            }
            makeWall();
        }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.gray);
        for (Position position : this.positions) {
            if (handler.getVisiblePositions().getPositions().contains(position)) {
                g.fillRect(position.getX(), position.getY(), Game.TILE_SIZE, Game.TILE_SIZE);
            }
        }
    }

    private void makeWall() {
        List<Position> positions = makeWallPositions();
        positions.add(getPosition());
        while (!PositionUtil.arePositionsFree(positions, handler) || handler.getVisiblePositions().arePositionsInVisiblePositions(positions)) {
            setPosition(PositionUtil.getRandomFreePosition(handler));
            positions = makeWallPositions();
            positions.add(getPosition());
        }
        this.positions = positions;
    }

    public void restart() {
        positions.clear();
        setPosition(PositionUtil.getRandomFreePosition(handler));
        makeWall();

    }

    private List<Position> makeWallPositions() {
        List<Position> positions = new ArrayList<>();
        Position newPositionE = new Position(getPosition().getX() + Game.TILE_SIZE, getPosition().getY());
        Position newPositionS = new Position(getPosition().getX(), getPosition().getY() + Game.TILE_SIZE);
        Position newPositionES = new Position(getPosition().getX() + Game.TILE_SIZE, getPosition().getY() + Game.TILE_SIZE);
        positions.add(newPositionE);
        positions.add(newPositionS);
        positions.add(newPositionES);
        return positions;
    }

    public List<Position> getPositions() {
        return positions;
    }
}
